(defproject repl "0.1.0-SNAPSHOT"
  :description "An example how to integrate with the Clojure REPL."
  :url "https://gitlab.com/julianthome/clojure-based-repl"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [com.bhauman/rebel-readline "0.1.4"]]
  :main ^:skip-aot repl.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  )
