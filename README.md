# Clojure-based REPL

A sample project to illustrate how to integrate applications with the Clojure
REPL `clj`. This project requires [Leiningen](https://leiningen.org). You can
spin up the REPL by running `lein trampoline run`. In case you would like to
build a standalone `.jar`, just run `lein uberjar`.

## License

Copyright © 2019, Julian Thome <julian.thome.de@gmail.com>

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
