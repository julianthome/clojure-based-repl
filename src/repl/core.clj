(ns repl.core
  (:require [clojure.main])
  (:require [rebel-readline.clojure.main])
  (:gen-class))

(defn myfunc [] (println "hello"))

(defn -main
  "Main"
  [& args]
  (rebel-readline.core/with-line-reader
    (rebel-readline.clojure.line-reader/create
     (rebel-readline.clojure.service.local/create))
    (clojure.main/repl
     :prompt (fn [])
     :need-prompt (fn [] false)
     :read (rebel-readline.clojure.main/create-repl-read)))
  )
